import pymysql


class DataBase:
    def __init__(self):
        self.connection = pymysql.connect(
            host='localhost',
            user='root',
            password='',
            db='bikecenter_bd'
        )

        self.cursor = self.connection.cursor()
        print('Conexión exitosa')

# TODO: Aqui se piden todos los usuarios de la base de datos
    def get_clients(self):
        sql = 'select  numero_documento,  nombre, apellido, user, password, direccion, tip_bike from cliente'
        try:
            self.cursor.execute(sql)
            return self.cursor.fetchall()

        except Exception as e:
            raise

# TODO: Aqui se muestra el usuario dependiendo el numero de documento
    def get_client(self, numero_documento):
        sql = 'select  numero_documento,  nombre, apellido, user, password, direccion, tip_bike from cliente where numero_documento = {}'.format(
            numero_documento)

        try:
            self.cursor.execute(sql)
            return self.cursor.fetchone()

        except Exception as e:
            raise

# TODO: Aqui se insertan los datos de un nuevo usuario
    def insert_client(self, numero_documento,  nombre, apellido, user, password, direccion, tip_bike):
        sql = f'INSERT INTO cliente (numero_documento, nombre, apellido, user, password, direccion, tip_bike) VALUES(\'{numero_documento}\', \'{nombre}\', \'{apellido}\', \'{user}\', \'{password}\', \'{direccion}\', \'{tip_bike}\')'
        print(sql)
        try:
            self.cursor.execute(sql)
            self.connection.commit()

        except Exception as e:
            raise

    def insert_Bike(self, id_bike,  marca, tipo):
        sql = f'INSERT INTO bike (id_bike, marca, tipo) VALUES(\'{id_bike}\', \'{marca}\', \'{tipo}\')'
        print(sql)
        try:
            self.cursor.execute(sql)
            self.connection.commit()

        except Exception as e:
            raise
# TODO: Aqui se borra un usuario dependiendo el numero de documento

    def delete_Client(self, numero_documento):
        sql = f'DELETE FROM cliente WHERE numero_documento = (\'{numero_documento}\')'
        print(sql)
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception as e:
            raise

    def update_client(self,  nombre, id):
        sql = f'update cliente set nombre=\'{nombre}\' where numero_documento=(\'{id}\')'
        print(sql)
        try:
            self.cursor.execute(sql)
            self.connection.commit()
        except Exception as e:

            raise


db = DataBase()
print(db.get_clients())
db.delete_Client(1)
db.insert_Bike(2, "SantaCruz", "MTB")
db.insert_client(7, 'CarlOS', 'Otalora', 'Carlitos',
                 'ajfuol', 'Crr 45 #45-46', 1)
db.update_client("William",7)